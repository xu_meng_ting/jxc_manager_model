--		进销存管理系统
create database Supermarket
go
use Supermarket
go

--进货
--    进货信息表：货号、商品名称、进价、数量、规格、供货商、供货商电话
create table Merchandise(
MerchandiseId  int primary key identity (1,1),
MerchandiseName   varchar(30) not null,
MerchandisePPrice   money,
MerchandiseNumber   varchar(30) not null,
MerchandiseSpecification   varchar(30) not null,
Merchandisesupplier   varchar(30) not null,
MerchandiseSPhone   varchar(30) not null
)
go
--    进货种类表：自增id、种类名称、种类数量、规格、单价
create table Kind(
KindId int primary key identity(1,1),
KindName varchar(20) not null,
KindNumber varchar(20) not null,
KindSpecification varchar(30) not null,
KindPrice varchar(20) not null
)
go
    
--    销售信息表：自增id、销售货号、零售价、数量、规格
create table Market(
MarketId int primary key identity(1,1),
MerchandiseId  int not null,
MerchandiseSPrice   money,
MerchandiseNumber   varchar(30) not null,
MerchandiseSpecification   varchar(30) not null
)
go


--    仓库表：商品货号、自增id、仓库名称、库存数量
create table Warehouse(
WarehouseId int primary key identity(1,1),
WarehouseName varchar(20) not null,
MerchandiseId  int not null,
WarehouseNumber varchar(20) not null
)
go

--    客户表：自增id、客户姓名、  客户电话、收货地址
create table Client(
ClientId int primary key identity(1,1),
ClientName varchar(20) not null,
ClientPhone varchar(20) not null,
ClientAddress varchar(30) not null
)
go
--    退货表： 自增id、退货商品ID，商品名称，客户ID，退货时间，退货数量，退货原因
create table Returnable(
ReturnableId int primary key identity(1,1),
ReturnableMId int not null,
ReturnableCId int not null,
MerchandiseName   varchar(30) not null,
ReturnableNumber varchar(20) not null,
ReturnableTime smalldatetime,
ReturnableReason varchar(100) not null
)
go


--    仓库调拨表：调拨ID，商品ID，商品名称，调出仓库，调入仓库，调动数量

  create table Allocated(
  AllocatedId int primary key identity(1,1),
  MerchandiseId  int not null,
  WarehouseName varchar(20) not null,
  AllocatedCallout varchar(20) not null,
  AllocatedCallin varchar(20) not null,
  AllocatedNumber varchar(20) not null
  )
  go

 


