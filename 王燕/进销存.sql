-- 创建客户信息表
create table UserInfo(
	UserId int primary key identity(1, 1), -- 账户编号
	UserCode varchar(20) not null, -- 身份证号码
	UserPhone varchar(20) not null, -- 电话号码
	UserName varchar(20) not null, -- 客户姓名
	UserPassword varchar(30) not null, -- 客户密码
);
--创建库存表
create table InventoryInfo(
    ProductName varchar(10) primary key,--产品名称
	InventoryTime varchar(10) not null,--进货时间
	PurchaseAmount varchar(10) not null,--进货总数
	ShipmentAmount varchar(10) not null,--出货总数
);

--创建商品信息表
create table GoodsInfo(
     GoodsSortCoding int primary key identity(1,1),--商品种类编码
	 GoodsSortName varchar(50) not null,--商品种类名称
	 GoodsPlace varchar(30) not null,--商品地点
	 GoodsModel varchar(10) not null,--商品型号
	 GoodsRetailPrice varchar(10)  not null,--商品零售价 
	 GoodsSupplier varchar(40) not null,--商品供应商
);
--创建采购信息表
create table  PurchaseInfo(
     BuyerName varchar(10) primary key,--采购员姓名
	 PurchaseId varchar(40) not null,--采购编号
     PurchaseAmount varchar(10) not null,--采购数量
	 PurchasePrice varchar(10) not null,--采购单价
	 PurchaseMount varchar(10) not null,--采购金额
);
--创建销售信息表
create table SalesInfo(
     SalesMan varchar(10) primary key,--销售员姓名
	 SalesAmount varchar(10) not null,--销售数量
	 SalesPrice varchar(10) not null,--销售单价
	 SalesMount varchar(10) not null,--销售金额
);
--创建交易信息表
create table TransactionInfo(
	ExchangeMount varchar(20) primary key,--交易金额
	ExchangeTime smalldatetime not null, -- 交易时间 
    ExchangeAmount varchar(10) not null,--交易数量
); 
--创建仓库信息表
create table WareHouseInfo(
    WareHouseNumber varchar(100) primary key,--仓库编号
	InventoryQuantity varchar(20) not null,--入库数量
	InventorySort varchar(50) not null,--入库种类
	InventoryTime varchar(20) not null,--入库时间
);