create database PSS
go
use PSS
go
--1
--供应商表
--供应商id ，供应商姓名,联系电话,公司名称，公司地址
create table Supplier
(
	SupplierId                 int primary key identity(1,1)   not null,
	SupplierName               varchar(50)                     not null,
	SupplierPhone              varchar(20)                     not null,
	SupplierIdCompanyName      varchar(50)                     not null,
	SupplierIdCompanyAddress   varchar(50)                     not null,
)
--2
--客户表 
--客户id，客户姓名，联系电话，客户公司，客户公司地址
create table Client
(
	ClientId                  int primary key identity(1,1)   not null,
	ClientName                varchar (50)                    not null,
	ClientPhone               varchar(20)                     not null,
	ClientCompanyName         varchar(50)                     not null,
	ClientCompanyAddress      varchar(50)                     not null,

)
--3
--商品表
--商品Id,商品名称，价格，
create table Commodity
(
	CommodityId        int primary key identity(1,1)       not null,
	CommodityName      varchar(30)                         not null,
	CommodityPrice     money                               not null,
)
--4
--采购表
--采购单号,采购商品Id，供应商，采购数量，采购时间,总价格，采购状态(1,未完成 2 完成)
create table Purchase
(
	 PurchaseNo             int primary key           not null,
	 CommodityId            int                       not null,
	 SupplierId             int                       not null,
	 PurchaseCount          int                       not null,
	 PurchaseTime           smalldatetime             not null,
	 PurchasePrice          money                     not null,
	 PurchaseState          int                       not null,
)
--5
--供应商往来明细表
--供应商往来明细id，应支付金额，发票号码,入账日期，发票金额,填写发票日期，支付状态（0未付，1已付款）
create table SupplierBillDetail
(
	SupplierBillDetailId       int primary key identity(1,1)        not null,
	Pay	                       money                                not null,
	CheckNumber                varchar(30)                          not null,
	EnterDate                  smalldatetime                        not null,
	CheckMoney                 money                                not null,
	WriteCheckTime             smalldatetime                        not null,
	PayState                   int                                  not null,
)
--6
--客户往来明细表
--客户往来明细Id,应收款金额，发票号码，收款日期，发票金额，填写发票日期，收款状态（0未收款，1已收款）
create table ClientIntercourse
(
	ClientIntercourseId         int primary key identity(1,1)       not null,
	Receipt                     money                               not null,
	CheckNumber                 varchar(30)                         not null,
	ReceiptTime                 smalldatetime                       not null,
	CheckMoney                  money                               not null,
	WriteCheckTime              smalldatetime                       not null,
	ReceiptState                int                                 not null,
)

--7
--销售表
--销售单号id,客户id,送货地址,销售日期,总金额,销售状态（(1,未完成 2 完成)）
create table Sell
(
	SellNo             varchar(20) primary key              not null,
	ClientId           int                                  not null,
	SellAddress        VARCHAR(30)                          not null,
	SellTime           smalldatetime                        not null,
	SellMoney          money                                not null,
	SellState          int default(1)                       not null,
)
--8
--退货表
--退货单号，客户id，销售单号，退货物品，退货数量，退货日期，总金额，退货状态（(1,未完成 2 完成)）
create table SellReturns
(
	SellReturnsNo         varchar(20) primary key           not null,
	ClientId              int                               not null,
	SellNo                varchar(20)                       not null,
	CommodityId           int                               not null,
	SellReturnsCount      int                               not null,
	SellReturnsTime       smalldatetime                     not null,
	SellReturnsMoney      money                             not null,
	SellReturnsState      int default(1)                    not null,
)
--9
--库存商品表
--库存商品Id,商品名称,商品数量，生产日期,、失效日期,
create table Inventory
(
	InventoryId            int primary key identity(1,1)    not null,
	InventoryName          varchar(30)                      not null,
	InventoryCount         int                              not null,
	BornTime               smalldatetime                    not null,
	ExpiredTime            smalldatetime                    not null,
)
--10
-- 商品入库表
--	入库编号，商品名称，入库数量，入库时间，
create table CommodityEntrepot
(
	EntrepotNo                 varchar(20) primary key      not null,
	CommodityName              varchar(20)                  not null,
	EntrepotCount              int                          not null,
	EntrepotTime               smalldatetime                not null,
)

--11
--商品出库表
--出库编号，商品名称，出库数量，出库时间
create table CommodityOutbound
(
	CommodityOutboundNo        varchar(20) primary key      not null,
	CommodityName              varchar(20)                  not null,
	OutboundNumber             int                          not null,
	OutboundTime               smalldatetime                not null,
)
--12
--部门表
-- 部门编号，部门名称，部门主管，部门主管电话
create table Department
(
	DepartmentId               int primary key identity(1,1) not null,
	DepartmentName             varchar(10)                   not null,
	DepartmentManager          varchar(20)                   not null,
	DepartmentManagerPhone     varchar(20)                   not null,
)
--13
--职员表
--职员id，职员姓名，职员部门,性别(1为男，0为女)，电话，
create table Worker
(
	WorkerId              int primary key identity(1,1)      not null,
	WorkerName            varchar(20)                        not null,
	DepartmentId          int                                not null,
	sex                   int                                not null,
	WorkerPhone           varchar(20)                        not null,
)